'/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
'名　称: WEBメールコントローラ
'機　能: ㈰自動ログイン
'        ㈪ページ自動リロード
'        ㈫新着メール通知
'        ㈬全員返信
'使い方: 
'作成者: 孫
'作成日: 2009/03/27 16:00
'履　歴: Ver.       日　付             修正者         内容
'----------------------------------------------------------------------
'        1.0        2009/03/27         孫             新規作成
'        1.1        2009/04/02         孫             ページ自動更新機能を追加
'        1.2        2009/04/03         孫             新着メール通知追加
'        1.3        2009/04/07         孫             全員返信機能追加
'        1.31       2009/04/07         孫             異常終了時メッセージを表示
'        1.32       2009/04/08         孫             メール件数取得バグ修正
'        1.33       2009/04/08         孫             全員返信文言を赤に
'        1.4        2009/04/09         孫             操作パネルを追加
'        2.00       2009/04/10         孫             バックアップ機能追加
'        2.10       2009/04/13         孫             送信箱バックアップ追加
'        2.11       2009/04/13         孫             バック修正:最後のページがバックアップされない
'        2.12       2010/08/09         孫             IE8対応(終了時ウィンドウクローズしないように)
'/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
'On Error Resume Next

'#################################################################
'                          定数定義
'#################################################################
'ログイン情報
Dim ARR_LOGIN_INFO,RELOAD_INTEV

'***********************************************
'               ユーザ設定領域
'***********************************************
'㈰ログイン情報
ARR_LOGIN_INFO = Array(Array("1","WEBメール","http://10.145.12.230/mail/login","******",  "****","userid","password",""))
'㈪自動更新間隔(分)
RELOAD_INTEV = 3
'㈫ループ間隔(秒)
LOOP_INTEV = 2
'㈬ウインドサイズ
WINDOW_WIDTH = 1024
WINDOW_HEIGHT = 700

'***********************************************
'               システム設定領域
'***********************************************
MAIL_TYPE_SEND = "SEND"
MAIL_TYPE_REC = "REC"
MAIL_PAGE_COUNT = 30
BACKUP_FOLDER = "backup"
BACKUP_FOLDER_SEND = "send"
BACKUP_FOLDER_REC = "rec"
BACKUP_POINT_FILE_SEND = "bkPointSend.ini"
BACKUP_POINT_FILE_REC = "bkPointRec.ini"
BACKUP_RECORDS_FILE_SEND = "send_mail.csv"
BACKUP_RECORDS_FILE_REC = "rec_mail.csv"
BACKUP_RECORDS_DELIMITER = ","


'#################################################################
'                          変数定義
'#################################################################
Dim arrLoginInfo
Dim WshShell
Dim IE
Dim intIntev1,intIntev2
Dim intTimer
Dim intLoginResult
Dim intArrUbound
Dim strTemp,strTemp2,intTemp,strUrlIndex
Dim Fs,info
Dim strUrl,strLoginId,strLoginPswd
Dim strOldMailDate,intOldMailCount,intNewMailCount
Dim objNewMail,objControlMenu

'#################################################################
'                          変数初期化
'#################################################################

'待機時間
intIntev1 = 500
intIntev2 = 100

arrLoginInfo = ARR_LOGIN_INFO

'#################################################################
'                         オブジェクト作成
'#################################################################
set WshShell = WScript.CreateObject("WScript.Shell")
Set Fs = WScript.CreateObject("Scripting.FileSystemObject")

'#################################################################
'                         IE起動、画面表示
'#################################################################
Set IE = RunIE(arrLoginInfo(0)(2))

'#################################################################
'                         ログイン
'#################################################################
intLoginResult = TestLogin(IE,arrLoginInfo(0)(3), _
                              arrLoginInfo(0)(4), _
                              arrLoginInfo(0)(5), _
                              arrLoginInfo(0)(6), _
                              arrLoginInfo(0)(7))

'全員返信ボタンを追加
Call AddReplyAllButton(IE)
Set objControlMenu = CreateControlMenu(IE)
intOldMailCount = GetMailCount(IE)

'#################################################################
'                         定期リロード
'#################################################################
Do while 1
  '  WScript.Sleep RELOAD_INTEV*60000
    oldTime = now
    'コントロールメニュー動作検知
    Do while 1
        WScript.Sleep LOOP_INTEV*1000
        '停止ボタン押下
        If objControlMenu.rows(0).cells(1).innerText = "4" Then
            oldTime = now
        End If
        '更新ボタン押下
        If objControlMenu.rows(0).cells(2).style.color = "red" Then
            exit do
        End If
        '終了ボタン押下
        If objControlMenu.rows(0).cells(3).style.color = "red" Then
            Call Logout(IE)
            Set IE = Nothing
            set WshShell = Nothing
            Wscript.Quit(0)
        End If
        'バックアップボタン押下
        If objControlMenu.rows(1).cells(0).style.color = "red" Then
            If Msgbox("バックアップを始めると、すべての未読メールは既読になります。" & vbCrLf & _
                   "開始してよろしいですか",vbYesNo,"バックアップ") = vbYes Then
                '受信箱
                bkRecNum = BackUpMail(IE,MAIL_TYPE_REC)
                bkSendNum = BackUpMail(IE,MAIL_TYPE_SEND)

                Msgbox "バックアップが完了しました" & vbCrLf & _
                       "****************************" & vbCrLf & _
                       "受信箱: " & bkRecNum & "件"  & vbCrLf & _
                       "送信箱: " & bkSendNum & "件"  & vbCrLf & _
                       "****************************" & vbCrLf _
                       ,vbOKOnly,"バックアップ完了"
            End If
            exit do
        End If

        '自動更新
        If DateDiff("n",oldTime,now) >= RELOAD_INTEV Then
           exit do
        End If
    Loop
    'ページ更新
    Call ReloadIE(IE)
    '全員返信ボタンを追加
    Call AddReplyAllButton(IE)
    Set objControlMenu = CreateControlMenu(IE)
    '受信メール数取得
    intNewMailCount = GetMailCount(IE)
    '新メールあり
    If intNewMailCount > intOldMailCount Then
        If msgbox("新着メール:" & Cstr(intNewMailCount-intOldMailCount) & "通",vbYesNo,"新着メールあり") = vbYes Then
            IE.Document.parentWindow.focus()
        End If
        intOldMailCount = intNewMailCount
    End If
    'ブラウザが閉じられたら、終了
    If Err.Number <> 0 Then
        Set IE = Nothing
        set WshShell = Nothing
        msgbox "WEBメールコントローラが異常終了しました。"
        Wscript.Quit(0)
    End If
Loop
Set IE = Nothing
set WshShell = Nothing
Wscript.Quit(0)



'/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
'
'                         ローカル関数
'
'/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/


'#################################################################
'                    IEロード完了まで待機
'#################################################################
Sub WaitIEComplete(IE)
    'IE状態
    while IE.busy
    	WScript.Sleep intIntev2
    wend
    while IE.Document.ReadyState <> "complete"
    	WScript.Sleep intIntev2
    wend
    'フレーム
    If IE.Document.frames.length > 1 Then
        For i=0 To IE.Document.frames.length - 1
            while IE.Document.frames(i).Document.ReadyState <> "complete"
            	WScript.Sleep intIntev2
            wend
        Next
    End If
End Sub

'#################################################################
'                    IE起動、初期画面表示
'#################################################################
Function RunIE(strURL)
    Dim objIE
  'IE起動、画面表示
    Set objIE = WScript.CreateObject("InternetExplorer.Application")
    With objIE
    	.Navigate(strURL)
    	.Width = WINDOW_WIDTH
    	.Height = WINDOW_HEIGHT
    	.Top = 0
    	.Left = 0 
    	.Visible = true
    	.MenuBar = false
    	.StatusBar = false
    	.ToolBar = false
    End With

    WScript.Sleep intIntev1

    Call WaitIEComplete(objIE)
    
    Set RunIE = objIE
End Function

'#################################################################
'                    IEリロード
'#################################################################
Sub ReloadIE(objIE)
    objIE.Navigate(objIE.Document.url)

    WScript.Sleep intIntev1

    Call WaitIEComplete(objIE)
    
    'ブラウザが閉じられたら、終了
    If Err.Number <> 0 Then
        Set IE = Nothing
        set WshShell = Nothing
        msgbox "WEBメールコントローラが異常終了しました。"
        Wscript.Quit(0)
    End If
End Sub



'#################################################################
'                        ログイン測定
'#################################################################
Function TestLogin(IE,id,pswd,idBox,pswdBox,submitFunc)
    Dim intTimer
    
    IE.Document.parentWindow.focus()
    
    'ログイン
    IE.Document.getElementsByName(idBox)(0).value = id
    IE.Document.getElementsByName(pswdBox)(0).value = pswd
    IE.Document.parentWindow.focus()
    intTimer = now
    'サブミット
    If submitFunc = "" Then
        IE.Document.getElementsByTagName("form")(0).submit()
    Else
        IE.Document.parentWindow.execScript(submitFunc)
    End If

    Call WaitIEComplete(IE)
    
    'ログイン結果を判別
    If Instr(IE.Document.title,"Auth") > 0 then
        MsgBox "ログイン失敗:" & vbCrLf & "「続ける」をクリックした後、「OK」ボタンを押下してください。"
    End If
    
    Call WaitIEComplete(IE)

    TestLogin =0
End Function

'#################################################################
'                        ログアウト
'#################################################################
Sub Logout(IE)
    Dim objDoc
    Dim tmpUrl
    Dim tmpScript
   
    tmpUrl = IE.document.frames(0).document.forms(0).action_org.value & "?change=Logout"
    IE.Navigate("http://10.145.12.230/" & tmpUrl)
    'IE.Document.parentWindow.location.href = IE.document.frames(0).document.forms(0).action_org.value & "?change=Logout"
    'IE.Document.frames(0).openLogout()
    'IE状態
    while IE.busy
    	WScript.Sleep intIntev2
    wend
    while IE.Document.ReadyState <> "complete"
    	WScript.Sleep intIntev2
    wend
    '警告メッセージを出さずに閉じる
    'tmpScript = "window.open('', '_parent').opener = window).close();"
    'IE.Document.parentWindow.execScript(tmpScript)
    IE.Quit()
    'IE.Document.parentWindow.opener = ""
    'IE.Document.parentWindow.close()
End Sub


'#################################################################
'                   メール件数確認
'#################################################################
Function GetMailCount(IE)
    Dim objDocMsgList,objTblMsgList
    'メールリストフレーム取得
   Set objDocMsgList = IE.document.frames(1).document
   'テーブルオブジェクト取得
   Set objTblMsgList = objDocMsgList.getElementsByTagName("table")

    '一行目を取得
   GetMailCount = GetNumberFromString(objTblMsgList(0).rows(0).cells(0).getElementsByTagName("font")(0).innerText)
End Function

'#################################################################
'                   受信メール確認
'#################################################################
Function GetNewMail(IE)
    Dim objDocMsgList,objTblMsgList
    'メールリストフレーム取得
   Set objDocMsgList = IE.document.frames(1).document
   'テーブルオブジェクト取得
   Set objTblMsgList = objDocMsgList.getElementsByTagName("table")

    '一行目を取得
   Set GetNewMail = objTblMsgList(0).rows(3)
End Function

'#################################################################
'                   文字列から数字を抽出
'#################################################################
Function GetNumberFromString(str)
    Dim re

    Set re = New regExp                             '正規表現オブジェクトを作成します。
    With re
        .IgnoreCase = True                            '大文字小文字を区別するよう設定します。
        .Global = True                                '最初の一致のみ検索するよう設定します。
        .Pattern = "[^0-9]"                           '正規表現パターンを設定します。
  End With
    GetNumberFromString = Clng(re.Replace(str, ""))
End Function

'#################################################################
'          メニューフレームのアイコンテーブル取得
'#################################################################
Function GetObjMenuTable(IE)
    Dim objDocMenu,objMenuTbl
    'メニューフレーム
    Set objDocMenu = IE.document.frames(0).document
    '外側テーブル
    Set objMenuTbl = objDocMenu.getElementsByTagName("table")
    '入れ子テーブル
    Set GetObjMenuTable = objMenuTbl(0).rows(0).cells(0).getElementsByTagName("table")
End Function
'#################################################################
'                   全員返信ボタン追加
'#################################################################
Sub AddReplyAllButton(IE)
    Dim objMenuTbl,objNewCell1,objNewCell2
   
    Set objMenuTbl = GetObjMenuTable(IE)
    Set objNewCell1 = objMenuTbl(0).rows(0).insertCell()
    objNewCell1.innerHtml = "<a href=""javascript:openAllReplyMsg();"" onMouseOver=""return setWindowStatus('全員返信');"" onMouseOut=""return setWindowStatus('全員返信');""><img src=""/webmail/image/jwmlrep.gif"" alt=""全員返信"" border=""0"">"
    Set objNewCell2 = objMenuTbl(0).rows(1).insertCell()
    objNewCell2.innerHtml = "<font color='red'>全員返信</font>"
End Sub

'#################################################################
'                   コントローラメニュー作成
'#################################################################
Function CreateControlMenu(IE)
    Dim objMenuTbl,objNewCell1,objNewCell2
    Set objMenuTbl = GetObjMenuTable(IE)
    Set objNewCell1 = objMenuTbl(0).rows(0).insertCell()
    objNewCell1.rowspan = "2"
    objNewCell1.width = "300"

    objNewCell1.innerHtml = "<table border=""2"" style=""background-color:#f1f1f1;font:normal bold 25px/25px webdings;color:blue;text-align:center;vertical-align:middle;"" alignt=""center"">" & _
                                "<tr style=""height:30px;font:normal bold 25px/25px webdings;color:blue"">" & _
                                    "<td rowspan='2' style=""font:normal bold 15px serif;color:#000000;"">操作<br>パネル</td>" & _
                                    "<td style='width:30px;hight:25px;' title=""開始/一時停止"" onMouseOver=""this.style.cursor='pointer'"" onclick=""if(this.innerText==';'){this.innerText='4';}else{this.innerText=';';};"">;</td>" & _
                                    "<td title=""更新"" style='width:30px;hight:25px;' onMouseOver=""this.style.cursor='pointer'"" onclick=""if(this.style.color=='red'){this.style.color='blue';}else{this.style.color='red';};"">q</td>" & _
                                    "<td title=""終了"" style='width:30px;hight:25px;' onMouseOver=""this.style.cursor='pointer'"" onclick=""if(this.style.color=='red'){this.style.color='blue';}else{this.style.color='red';};"">r</td>" & _
                                "</tr>" & _
                                "<tr style=""height:30px;"">" & _
                                    "<td title=""バックアップ"" style='width:30px;hight:25px;' onMouseOver=""this.style.cursor='pointer'"" onclick=""if(this.style.color=='red'){this.style.color='blue';}else{this.style.color='red';};"">&#0210</td>" & _
                                    "<td>&nbsp;</td>" &_
                                    "<td>&nbsp;</td>" &_
                                "</tr>" &_
                            "</table>"
    Set CreateControlMenu = objNewCell1.getElementsByTagName("table")(0)
End Function

'#################################################################
'                   メールバックアップ
'#################################################################
Function BackUpMail(IE,mailType)
    Dim objFs,objBkFile
    Dim objFrmMsgList,objFrmMsg
    Dim objTblMsg
    Dim arrMsgId,intStartPage,msgIdList,MsgIdStart
    Dim MsgBackupPoint,MsgBackupCount
    Dim oldMsgDate,newMsgDate

    'メールリストフレーム取得
    Set objFrmMsgList = IE.document.frames(1)
    Set objFrmMsg = IE.document.frames(2)
    'メールボックス切り替え
    Call SwitchMailBox(IE,mailType)
    
    'バックアップ台帳ファイルオープン
    Set objFs = WScript.CreateObject("Scripting.FileSystemObject")
    Set objBkRecordsFile = OpenBackupRecordsFile(objFs,mailType)

    'パックアップ開始ポイント(メッセージID)
    MsgBackupPoint = GetBackupPoint(objFs,mailType)
    MsgIdStart = MsgBackupPoint + 1
    
    '受信日時降順に並べ替え
    Call objFrmMsgList.sortIt("5")
    Call WaitIEComplete(IE)
    '受信箱は2回 送信箱は1回で降順となる
    If mailType = MAIL_TYPE_REC Then
        Call objFrmMsgList.sortIt("5")
        Call WaitIEComplete(IE)
    End If

    'バックアップ開始ページ取得
    intStartPage = GetBackUpStartPage(IE,MsgIdStart)
    MsgBackupCount = 0
    oldMsgDate = ""
    'バックアップ開始(開始ページから1ページ目まで)
    For i = intStartPage to 1 step -1
    
        Call objFrmMsgList.page_send(i,(i-1)*MAIL_PAGE_COUNT,i*MAIL_PAGE_COUNT)
        Call WaitIEComplete(IE)
        
        msgIdList = objFrmMsgList.viewmsgs
        arrMsgId = Split(msgIdList,",")
        arrMsgId = ArrayReverse(arrMsgId)
        For each k in arrMsgId
            If Clng(k) >= Clng(MsgIdStart) Then
                Call objFrmMsgList.openMsg(k,"0")
                Call WaitIEComplete(IE)
                Call objFrmMsg.openAddress("[to][cc]")
                Call WaitIEComplete(IE)
                'メール内容取得(送信者〜件名)
                Set objTblMsg = objFrmMsg.document.getElementsByTagName("table")(0)
                msgSender = objTblMsg.Rows(0).Cells(1).innerText
                msgDate = objTblMsg.Rows(3).Cells(1).innerText              '台帳レコード用
                newMsgDate = Replace(Left(msgDate,10),"/","")               'バックアップファイル名用
                msgSubject = objTblMsg.Rows(4).Cells(1).innerText
                '日付が変わった場合、新しいファイルを作成
                If newMsgDate <> oldMsgDate Then
                    Set objBkFile = OpenBackupFile(objFs,newMsgDate,mailType)
                    oldMsgDate = newMsgDate
                End If
                
                objBkFile.WriteLine "/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/"
                objBkFile.WriteLine "メールID: " & k
                objBkFile.WriteLine "/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/"

                objBkFile.WriteLine objFrmMsg.document.getElementsByTagName("body")(0).innerText
                
                'バックアップ台帳
                objBkRecordsFile.WriteLine k          & BACKUP_RECORDS_DELIMITER & _
                                           msgDate    & BACKUP_RECORDS_DELIMITER & _
                                           msgSender  & BACKUP_RECORDS_DELIMITER & _
                                           msgSubject & BACKUP_RECORDS_DELIMITER & _
                                           CreateBackupFileLink(newMsgDate,mailType)
                MsgBackupPoint = k
                MsgBackupCount = MsgBackupCount + 1
            End If
        Next
    Next
    Call SetBackupPoint(objFs,MsgBackupPoint,mailType)
    If MsgBackupCount > 0 Then
        objBkFile.Close
    End If
    objBkRecordsFile.Close
    Set objFs = Nothing
    '
   BackUpMail = MsgBackupCount

End Function

'#################################################################
'                   バックアップ開始ページ
'#################################################################
Function GetBackUpStartPage(IE,MsgIdStart)
    Dim objFrmMsgList
    Dim arrBkMsgId()
    Dim j,k,msgIdList,msgNum,msgPage
    Set objFrmMsgList = IE.document.frames(1)

    j=1
    msgIdList = ""
    msgNum = GetMailCount(IE)
    msgPage = msgNum/MAIL_PAGE_COUNT
    '1ページ目から対象開始ID以下のIDが現れるまで巡る
    Do While 1
        Call objFrmMsgList.page_send(j,(j-1)*MAIL_PAGE_COUNT,j*MAIL_PAGE_COUNT)
        Call WaitIEComplete(IE)

        msgIdList = msgIdList & "," & objFrmMsgList.viewmsgs
        arrMsgId = Split(msgIdList,",")
        '対象開始ID以下のIDが現れる
        If Clng(arrMsgId(Ubound(arrMsgId))) <= Clng(MsgIdStart) Then
            exit Do
        End If
        '最後のページ
        If j >= msgPage Then
            exit Do
        End If
        j = j + 1
    Loop
    
   
    GetBackUpStartPage = j
End Function

'#################################################################
'                   バックアップ開始ページ
'#################################################################
Function ArrayReverse(arr)
    dim arrNew()
    Dim arrLen
    arrLen = Ubound(arr)
    Redim arrNew(arrLen)
    
    For i=0 To arrLen
        arrNew(i) = arr(arrLen-i)
    Next
    ArrayReverse = arrNew
End Function

'#################################################################
'                          バックアップファイル作成
'#################################################################
Function OpenBackupFile(objFs,msgDate,mailType)
   'バックアップポイントファイル決定
    If mailType = MAIL_TYPE_SEND Then
        strFileName = BACKUP_FOLDER & "\" & BACKUP_FOLDER_SEND & "\" & msgDate & ".txt"
    Else
        strFileName = BACKUP_FOLDER & "\" & BACKUP_FOLDER_REC & "\" & msgDate & ".txt"
    End If
    Set OpenBackupFile = objFs.OpenTextFile(strFileName,8,1)
End Function

'#################################################################
'                          バックアップファイル作成
'#################################################################
Function CreateBackupFileLink(msgDate,mailType)
       'バックアップポイントファイル決定
    If mailType = MAIL_TYPE_SEND Then
        strFileName =  BACKUP_FOLDER_SEND & "\" & msgDate & ".txt"
    Else
        strFileName =  BACKUP_FOLDER_REC & "\" & msgDate & ".txt"
    End If
    CreateBackupFileLink = "=hyperlink(""" & strFileName & """)"
End Function
'#################################################################
'                 バックアップポイント作成
'#################################################################
Sub SetBackupPoint(objFs,msgId,mailType)
    Dim strFileName
    'バックアップポイントファイル決定
    If mailType = MAIL_TYPE_SEND Then
        strFileName = BACKUP_FOLDER & "\" & BACKUP_POINT_FILE_SEND
    Else
        strFileName = BACKUP_FOLDER & "\" & BACKUP_POINT_FILE_REC
    End If
    Set objBkPoint = objFs.OpenTextFile(strFileName,2,1)
    objBkPoint.writeline msgId
    
    objBkPoint.Close()
End Sub

'#################################################################
'                 バックアップポイント取得
'#################################################################
Function GetBackupPoint(objFs,mailType)
    Dim strFileName
    'バックアップポイントファイル決定
    If mailType = MAIL_TYPE_SEND Then
        strFileName = BACKUP_FOLDER & "\" & BACKUP_POINT_FILE_SEND
    Else
        strFileName = BACKUP_FOLDER & "\" & BACKUP_POINT_FILE_REC
    End If

    Set objBkPoint = objFs.OpenTextFile(strFileName,1,1)
    If objBkPoint.AtEndOfStream Then
        GetBackupPoint = 0
    Else
        GetBackupPoint = Clng(objBkPoint.readline)
    End If
    objBkPoint.Close()
End Function

'#################################################################
'                 メールボックス切り替え
'#################################################################
Sub SwitchMailBox(IE,mailType)
    Dim objSelect
    Dim mailBoxOp
    'メールボックス決定
    If mailType = MAIL_TYPE_SEND Then
        mailBoxOp = 1
    Else
        mailBoxOp = 0
    End If
    'メールボックスドロップリスト
    Set objSelect = IE.document.frames(0).document.getElementsByTagName("SELECT")(0)
    '変更
    If objSelect.options(mailBoxOp).selected = false THEN
        Call WaitIEComplete(IE)
        objSelect.options(mailBoxOp).selected = "selected"
        '切り替え
        ret = IE.document.frames(0).chFolder("",objSelect)
    End If
    Call WaitIEComplete(IE)
END Sub

'#################################################################
'                   バックアップメール台帳ファイル作成
'#################################################################
Function OpenBackupRecordsFile(objFs,mailType)
    Dim objFile
   'バックアップポイントファイル決定
    If mailType = MAIL_TYPE_SEND Then
        strFileName = BACKUP_FOLDER & "\" & BACKUP_RECORDS_FILE_SEND
    Else
        strFileName = BACKUP_FOLDER & "\" & BACKUP_RECORDS_FILE_REC
    End If
    If objFs.FileExists(strFileName) = false Then
        Set objFile = objFs.OpenTextFile(strFileName,8,1)
        objFile.WriteLine "メールID"      & BACKUP_RECORDS_DELIMITER & _
                          "送受信日時"    & BACKUP_RECORDS_DELIMITER & _
                          "送信者"        & BACKUP_RECORDS_DELIMITER & _
                          "件名"          & BACKUP_RECORDS_DELIMITER & _
                          "バックアップファイル"
    Else
        Set objFile = objFs.OpenTextFile(strFileName,8,1)
    End If
    Set OpenBackupRecordsFile = objFile
End Function
