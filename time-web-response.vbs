'/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
'/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

'#################################################################
'                          定数定義
'#################################################################
'URL情報
Dim ARR_URL
ARR_URL = Array("http://xxxxxxx",_
                "http://xxxxxxx",_
                "http://xxxxxxxx",)

'ログイン監視用
strPlusLoginUrl = "xxxxx_login.html"


'ログインID/PSWD
strId       = "xxxx"
strPass     = "xxxx"


'結果ファイル、メールテンプレートファイル
CONST LOG_FILE_NAME = "監視結果_"

'エントリURL
CONST URL_ENTER = "http://www.google.co.jp/"


'報告時刻(分)
CONST REPORT_POINT = "00"

'監視間隔(分単位)
CONST MONI_INTEV = 5

'#################################################################
'                          変数定義
'#################################################################
Dim WshShell
Dim IE
Dim intIntev1,intIntev2
Dim intTimer
Dim intLoginResult,intAvgLogin
Dim arrTmResult(),arrSumTmResult(),arrAvgTmResult(),intTestCounter
Dim arrMoniAspName,arrMoniAspParm
Dim arrSubjectParm(),arrBodyParm()
Dim intArrUbound
Dim strTemp,strTemp2,intTemp,strUrlIndex
Dim strChkTime
Dim Fs,info
Dim intReportFlg,arrReportPoint,intRebootFlg
Dim ReRunTime

'#################################################################
'                          変数初期化
'#################################################################

'待機時間
intIntev1 = 500
intIntev2 = 200

'再度レスポンスチェック時間(秒)
ReRunTime = 30

intArrUbound = Ubound(ARR_URL)

'測定結果用
Redim arrTmResult(intArrUbound)
Redim arrSumTmResult(intArrUbound)
Redim arrAvgTmResult(intArrUbound)

For i=0 To intArrUbound
    arrTmResult(i) = 0
    arrSumTmResult(i) = 0
    arrAvgTmResult(i) = 0
Next

intTestCounter = 0
intReportFlg = -1
intRebootFlg = 0

arrReportPoint = split(REPORT_POINT,",")

'#################################################################
'                          実行開始確認
'#################################################################
If MsgBox("定点監視を実施しますか", VbOKCancel, "定点監視") <> vbOK then
    Wscript.Quit(0)
end if

'#################################################################
'                         オブジェクト作成
'#################################################################

set WshShell = WScript.CreateObject("WScript.Shell")
Set Fs = WScript.CreateObject("Scripting.FileSystemObject")

'#################################################################
'                         ログファイル作成
'#################################################################
Set oTs1 = CreateLogFile(Fs)

'#################################################################
'                         監視ループ開始
'#################################################################
While(1)
    '測定時刻チェック(10秒ごとシステム日付をチェック)
    do
        WScript.Sleep 20000
        strTemp = now
    loop until Minute(strTemp) mod MONI_INTEV = 0
    
    intTemp = Minute(strTemp)
    For each strTemp2 In arrReportPoint
        If intTemp = Cint(strTemp2) Then
            intReportFlg = 1
            exit for
        End If
    Next

    '測定時刻(分まで)
    strChkTime = Mid(strTemp,12,len(strTemp)-14)
    
    'IE起動、画面表示
    Set IE = RunIE(URL_ENTER)
        
    'アクセス測定
    For i=0 To intArrUbound
        arrTmResult(i) = TestAccess(IE,ARR_URL(i))
        WScript.Sleep intIntev1 * 6
        If arrTmResult(i) >= ReRunTime Then
        	intRebootFlg = 1
        End If
    Next

    IE.quit
    Set IE = Nothing

    '2009/03/19追加 レスポンスが30秒以上だったら再度チェック
    'IE起動、画面表示
    If intRebootFlg = 1 Then
        intRebootFlg = 0
        Set IE = RunIE(URL_ENTER)
        For i=0 To intArrUbound
            arrTmResult(i) = TestAccess(IE,ARR_URL(i))
        Next
        IE.quit
        Set IE = Nothing
    End If

    
    intTestCounter = intTestCounter + 1
    
    For i=0 To intArrUbound
        arrSumTmResult(i) = arrSumTmResult(i) + arrTmResult(i)
        If arrTmResult(i) >= ReRunTime Then
            intRebootFlg = 1
        End If
    Next

    If intRebootFlg = 1 Then
         Msgbox "レスポンス測定結果が３０秒となりました。関西ＤＰＣへＷＥＢサーバＩＩＳ再起動の依頼をして下さい。"
    End If

    intRebootFlg = 0


    'ログ出力内容
    strTemp =  strChkTime
    For i=0 To intArrUbound
        strTemp = strTemp & vbTab & arrTmResult(i)
    Next
    
    '報告時刻になった場合の処理
    If intReportFlg = 1 Then
        '平均レスポンスを計算        
        For i=0 To intArrUbound
            arrAvgTmResult(i) = FormatNumber(arrSumTmResult(i)/intTestCounter,1,true)
        Next
        
        'ログ出力内容に平均値を付ける
        For i=0 To intArrUbound
            strTemp = strTemp & vbTab & arrAvgTmResult(i)
        Next
        
        '値クリア
        For i=0 To intArrUbound
            arrSumTmResult(i) = 0
        Next

        intTestCounter = 0
        intReportFlg = 0
        
    End If

    'ログ書き込み
    oTs1.writeline strTemp
    
    WScript.Sleep (MONI_INTEV-2) * 60000
Wend

Set IE = Nothing
set WshShell = Nothing
oTs1.Close

Wscript.Quit(0)



'/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
'
'                         ローカル関数
'
'/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/


'#################################################################
'                          ログファイル作成
'#################################################################
Function CreateLogFile(objFs)
    Dim strTemp
    strTemp = now
    strTemp = replace(strTemp,"/","")
    strTemp = replace(strTemp,":","_")
    strTemp = replace(strTemp," ","_")
    Set CreateLogFile = objFs.OpenTextFile(LOG_FILE_NAME&strTemp&".txt",8,1)
End Function


'#################################################################
'                    IE起動、初期画面表示
'#################################################################
Function RunIE(strURL)
    Dim objIE
  'IE起動、画面表示
    Set objIE = WScript.CreateObject("InternetExplorer.Application")
    With objIE
    	.Navigate(strURL)
    	.Width = 800
    	.Height = 200
    	.Top = 0
    	.Left = 0 
    	.Visible = true
    	.MenuBar = false
    	.StatusBar = true
    	.ToolBar = false
    End With

    WScript.Sleep intIntev1

    'IE状態
    while objIE.busy
    	WScript.Sleep intIntev1
    wend
    while objIE.Document.ReadyState <> "complete"
    	WScript.Sleep intIntev1
    wend
    
    Set RunIE = objIE
End Function

'#################################################################
'                   メニューアクセス測定
'#################################################################
Function TestAccess(IE,strUrlIndex)
    Dim strTemp,intTimer

    'OMCPlusログイン用は下で打刻開始
    If instr(1,strUrlIndex,strPlusLoginUrl) = 0 Then
	    intTimer = now
    End If

    IE.Navigate(strUrlIndex)
    
    'IE状態
    while IE.busy
    	WScript.Sleep intIntev2
    wend
    while IE.Document.ReadyState <> "complete"
    	WScript.Sleep intIntev2
    wend

    IE.Document.parentWindow.focus()

    'OMCPlusログイン用
    If instr(1,strUrlIndex,strPlusLoginUrl) <> 0 Then
    	IE.Document.form1.sid_input.value = strId
    	IE.Document.form1.sid.value = strId
    	IE.Document.form1.pw_input.value = strPass
    	IE.Document.form1.pw.value = strPass
    	IE.Document.parentWindow.focus()
    	intTimer = now
    	IE.Document.form1.submit

    	'IE状態
    	while IE.busy
    		WScript.Sleep intIntev2
    	wend

    	while IE.Document.ReadyState <> "complete"
    		WScript.Sleep intIntev2
    	wend

    End if

    TestAccess = DateDiff("s",intTimer,now)
End Function
